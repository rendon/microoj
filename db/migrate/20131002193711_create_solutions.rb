class CreateSolutions < ActiveRecord::Migration
  def change
    create_table :solutions do |t|
      t.integer :id_problem
      t.integer :id_user
      t.string :source_code
      t.string :language

      t.timestamps
    end
  end
end
