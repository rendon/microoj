require 'spec_helper'

describe "StaticPages" do
  describe "Home page" do
    before { visit root_path }
    it "shoud have the content 'Micro OJ" do
        expect(page).to have_content('Micro OJ')
    end

      it "should have the title" do
          expect(page).to have_title('MicroOJ | Inicio')
      end
  end

  describe "Help page" do
      before { visit help_path }
      it "should have the content 'Ayuda'" do
          expect(page).to have_content('Ayuda')
      end

      it "should have the title" do
          expect(page).to have_title('MicroOJ | Ayuda')
      end
  end

  describe "About page" do
      before { visit about_path }
      it "should have the content 'Acerca de'" do
          expect(page).to have_content('Acerca de')
      end

      it "should have the title" do
          expect(page).to have_title('MicroOJ | Acerca de')
      end
  end
end

