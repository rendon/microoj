require 'spec_helper'

describe "User pages" do

  subject { page }

  describe "profile page" do
      let(:user) { FactoryGirl.create(:user) }
      before { visit user_path(user) }

      it { should have_content(user.name) }
      it { should have_title(user.name) }
  end

  describe "signup page" do
    before { visit signup_path }

    it { should have_content('Nuevo usuario') }
    it { should have_title(full_title('Nuevo usuario')) }
  end

  describe "signup" do
      before { visit signup_path }
      let(:submit) { "Crear mi cuenta" }

      describe "with invalid information" do
          it "should not create a user" do
              expect { click_button submit }.not_to change(User, :count)
          end
      end

      describe "with valid information" do
          before do
              fill_in "Nombre",             with: "rendon"
              fill_in "Contraseña",         with: "sdflkj"
              fill_in "Confirmación",       with: "sdflkj"
          end

      end
  end
end

