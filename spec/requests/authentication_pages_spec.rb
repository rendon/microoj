require 'spec_helper'

describe "AuthenticationPages" do
    subject { page }
    
    describe "signin page" do
        before { visit signin_path }

        it { should have_content("Iniciar sesión") }
        it { should have_title(full_title("Iniciar sesión")) }
    end

    describe "signin" do
        before { visit signin_path }

        describe "with invalid information" do
            before { click_button "Iniciar sesión" }

            it { should have_title(full_title('Iniciar sesión')) }
            it { should have_selector('div.alert.alert-error', text: 'invalida')}

            describe "after visiting another page" do
                before { click_link "Inicio" }

                it { should_not have_selector('div.alert.alert-error') }
            end

        end


        describe "with valid information" do
            let(:user) { FactoryGirl.create(:user) }
            before do
                fill_in "Usuario",      with: user.name
                fill_in "Contraseña",   with: user.password
                click_button "Iniciar sesión"
            end

            it { should have_link('Perfil',     href: user_path(user)) }
            it { should have_link('Salir',      href: signout_path) }
            it { should_not have_link('Iniciar sesión', href: signin_path) }
        end


    end
end

