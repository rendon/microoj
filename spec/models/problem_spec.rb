require 'spec_helper'

describe Problem do
    before do
        @problem = Problem.new(title: "A + B",
                                description: "Datos A y B imprime A + B.",
                                author: 1)
    end

    subject { @problem }
    it { should respond_to(:title)          }
    it { should respond_to(:description)    }
    it { should respond_to(:author)    }

    it { should be_valid }

    describe "when title is not present" do
        before { @problem.title = " " }
        it { should_not be_valid }
    end

    describe "when description is not present" do
        before { @problem.description = " " }
        it { should_not be_valid }
    end


    describe "when problem title is already taken" do
        before do
            problem_with_same_title = @problem.dup
            problem_with_same_title.save
        end

        it { should_not be_valid }
    end
end
