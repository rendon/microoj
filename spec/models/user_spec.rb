require 'spec_helper'

describe User do
    before do 
        @user = User.new(name: "example_user",
                              password: "mypass",
                              password_confirmation: "mypass")
    end
    subject { @user }

    it { should respond_to(:name) }
    it { should respond_to(:password) }
    it { should respond_to(:password_digest) }
    it { should respond_to(:password_confirmation) }
    it { should respond_to(:authenticate) }
    it { should respond_to(:remember_token) }

    describe "when name is not present" do
        before { @user.name = " " }
        it { should_not be_valid }
    end

    describe "when name is not valid" do
        before { @user.name = "1user" }
        it { should_not be_valid }
    end

    describe "when name is too long" do
        before { @user.name = "a" * 17 }
        it { should_not be_valid }
    end

    describe "when name is too short" do
        before { @user.name = "a" }
        it { should_not be_valid }
    end

    describe "when password is not present" do
        before do 
            @user = User.new(name: "example_user",
                             password: " ",
                             password_confirmation: " ")
        end
        it { should_not be_valid }
    end

    describe "when password is too long" do
        before { @user.password = "a" * 17 }
        it { should_not be_valid }
    end

    describe "when password is too short" do
        before { @user.password = "a" * 5 }
        it { should_not be_valid }
    end

    describe "when password doesn't match confirmation" do
        before { @user.password_confirmation = "mismatch" }
        it { should_not be_valid }
    end


    describe "return value of authenticate method" do
        before { @user.save }
        let(:found_user) { User.find_by(name: @user.name) }

        describe "with valid password" do
            it { should eq found_user.authenticate(@user.password) }
        end

        describe "with invalid password" do
            let(:user_for_invalid_password) do
                found_user.authenticate("invalid")
            end

            it { should_not eq user_for_invalid_password }
        end
    end

end
