MicroOJ
=======
A Micro Online Judge for programming training. This application is the front end for my other project [MicroOJ_WS](https://bitbucket.org/rendon/microoj_ws).

This project doesn't aim to be a real online judge (not by the moment), this is simply a college project to put in practice the Web services topic. However I've added all the functionality I could until the deadline.

For more information see the project write up (in Spanish): [http://rendon.x10.mx/?p=865](http://rendon.x10.mx/?p=865).

License
=======
With exception of the employed components (wich have it's own licenses), this project is under GPLv3.
