Separando Dígitos
=================
Dado un número entero N (0 <= N <= 2^63) imprime sus dígitos separados por un espacio.

###Ejemplo de entrada

    12345

###Ejemplo de salida

    1 2 3 4 5


**NOTA**: Imprimir únicamente lo que se pide, no espacios ni saltos de línea extras.
