Anita Lava la Tina
==================
Quizás haz escuchado sobre los palíndromos. Son cadenas de texto que se leen
igual de inzquierda a derecha que de derecha a inzquierda. Por ejemplo, la
cadena "anitalavalatina" es un palíndromo, mientras que "otraspalabras" no lo
es.

En este problema tienes que determinar si una cadena de texto es o no un palíndromo.

###Entrada
La entrada consiste de una cadena de texto con una longitud N (1 <= N <= 1000),
que consisten únicamente de letras minúsculas y números.

###Salida
Si la cadena es palíndroma debes imprimir la cadeda "Si" (sin comillas), de lo
contrario debes imprimir "No" (sin las comillas).

###Ejemplo de entrada

    asdfghjkllkjhgfdsa

###Ejemplo de salida

    Si


**NOTA**: Imprimir únicamente lo que se pide, no espacios ni saltos de línea extras.
