import random

easy_tc = 3

for tc in xrange(easy_tc):
    n = random.randrange(1000) + 1
    print n, " : ", n * (n + 1) / 2

hard_tc = 2

for tc in xrange(hard_tc):
    n = random.randint(500000, 1000000) + 1
    print n, " : ", n * (n + 1) / 2


