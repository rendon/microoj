Bienvenida
==========
Haz decidido unirte al equipo de programadores que esta entrenando para participar en CONACUP este año y para ello el resto de los integrantes han diseñado un problema especialmente para ti y que servirá para ver si cuentas con lo mínimo para entrar, no te preocupes, no es tan difícil.

Dado un número N (1 <= N <= 1,000,000), imprime la suma de los números del 1 hasta N (inclusive).
###Ejemplo de entrada.

   5 

###Ejemplo de salida:

    15

**NOTA**: Imprimir únicamente lo que se pide, no espacios ni saltos de línea extras.
