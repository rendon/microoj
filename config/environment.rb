# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
MicroOJ::Application.initialize!

# Languages.
LANG_C      = 1
LANG_CPP    = 2
LANG_PYTHON = 3

WSDL_LOCATION = "http://localhost/microoj_ws/judge.php?wsdl"

ER_FAILED_TO_INSERT_RECORD  = -1
ER_AUTHENTICATION_FAILED    = -2
ER_TITLE_ALREADY_TAKEN      = -3

VALID_USER      = -100
INVALID_USER    = -101

# MySQL error codes.
ER_DUP_ENTRY = -1062

# Compilation constants.
SU_COMPILATION      = 0
ER_COMPILATION      = 1
NO_SUCH_SOLUTION    = 2




ERROR_MESSAGES = {
   "error_0" => "SU_COMPILATION",
   "error_1" => "ER_COMPILATION",
   "error_2" => "NO_SUCH_SOLUTION",
   "error_-100" => "VALID_USER",
   "error_-101" => "INVALID_USER",
   "error_-1" => "ER_FAILED_TO_INSERT_RECORD",
   "error_-2" => "ER_AUTHENTICATION_FAILED",
   "error_-3" => "ER_TITLE_ALREADY_TAKEN"
}

TEST_CASES_PER_PROBLEM = 5

AC = 0
WA = 1
TLE = 2

ADMIN_USER = "administrador"
