function changeEditorLanguage(selection)
{
    var lang = selection.options[selection.selectedIndex].value;
    var editor = ace.edit('editor');


    var sample = "";
    if (lang == 1) {
        var CCppMode = require("ace/mode/c_cpp").Mode;
        editor.getSession().setMode(new CCppMode());
        sample  = "#include <stdio.h>\n\n"
                + "int main(int argc, char **argv)\n"
                + "{\n\tprintf(\"Hola mundo!\\n\");\n"
                + "\treturn 0;\n}\n";
    } else if (lang == 2) {
        var CCppMode = require("ace/mode/c_cpp").Mode;
        editor.getSession().setMode(new CCppMode());
        sample  = "#include <iostream>\nusing namespace std;\n"
                + "int main(int argc, char **argv)\n"
                + "{\n\tcout << \"Hola mundo!\" << endl;\n"
                + "\treturn 0;\n}\n";
    } else if (lang == 3) {
        var PythonMode = require("ace/mode/python").Mode;
        editor.getSession().setMode(new PythonMode());
        sample = "print \"Hello World!\"";
    }

    var sourceCode = document.getElementById('source_code');
    sourceCode.value = editor.getSession().getValue();
    editor.getSession().setValue(sample);
}

function codeToTextarea()
{
    var editor = ace.edit('editor');
    var sourceCode = document.getElementById('source_code');
    sourceCode.value = editor.getSession().getValue();
    return true;
}

function changeEditorTheme(selection)
{
    var theme = selection.options[selection.selectedIndex].value;
    var editor = ace.edit('editor');

    if (theme == 1) {
        editor.setTheme('ace/theme/textmate');
    } else if (theme == 2) {
        editor.setTheme('ace/theme/monokai');
    }
}

function changeEditorKeybinding(selection)
{
    var mode = selection.options[selection.selectedIndex].value;
    var editor = ace.edit('editor');

    if (mode == 1) {
        editor.setKeyboardHandler('ace/keyboard/emacs');
    } else if (mode == 2) {
        editor.setKeyboardHandler('ace/keyboard/vim');
    }
}


