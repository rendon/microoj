module SessionsHelper

    def sign_in(user)
        remember_token = User.new_remember_token
        cookies.permanent[:remember_token] = remember_token

        #user.update_attribute(:remember_token, User.encrypt(remember_token))
        client = Savon::Client.new do
            wsdl.document = WSDL_LOCATION
        end
        request = client.request "Judge.update_user_remember_token" do
            soap.body = {
                :old => user.remember_token,
                :new => User.encrypt(remember_token)
            }
        end
        
        self.current_user = user
    end

    def current_user=(user)
        @current_user = user
    end

    def current_user
        remember_token = User.encrypt(cookies[:remember_token])
        @current_user ||= find_user_by_remember_token(remember_token)
    end

    def find_user_by_remember_token(remember_token)
        client = Savon::Client.new do
            wsdl.document = WSDL_LOCATION
        end
        request = client.request "Judge.find_user_by_remember_token" do
            soap.body = {
                :remember_token => remember_token
            }
        end

        response = request.to_hash[:judge_find_user_by_remember_token_response]
        user_data = response[:user]

        if user_data[:id].nil?
            nil
        else
            the_user = User.new
            the_user.id = user_data[:id]
            the_user.name = user_data[:name]
            the_user.password = user_data[:password]
            the_user.remember_token = user_data[:remember_token]
            the_user
        end
    end

    def signed_in?
        !current_user.nil?
    end

    def sign_out
        self.current_user = nil
        cookies.delete(:remember_token)
    end
end
