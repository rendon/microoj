class UsersController < ApplicationController
    def new
        @user = User.new
    end

    def create
        @user = User.new(user_params)
        if @user.valid?
            client = Savon::Client.new do
                wsdl.document = WSDL_LOCATION
            end

            name = params[:user][:name]
            password = params[:user][:password]

            url = SecureRandom.urlsafe_base64
            remember_token = Digest::SHA1.hexdigest(url.to_s)
            request = client.request "Judge.create_user" do
                soap.body = {
                    :user => name,
                    :password => password,
                    :remember_token => remember_token
                }
            end

            response = request.to_hash[:judge_create_user_response]
            id = response[:return].to_i

            if id > 0
                flash[:success] = "Bienvenido a Micro OJ"

                request = client.request "Judge.get_user_data" do
                    soap.body = {
                        :user => name,
                        :password => password
                    }
                end

                response = request.to_hash[:judge_get_user_data_response]
                user_data = response[:user]
                user = User.new
                user.id = user_data[:id]
                user.name = user_data[:id]
                user.password = user_data[:password]
                user.remember_token = user_data[:remember_token]

                sign_in user
                redirect_to root_path
            else
                render 'new'
            end

        else
            render 'new' # Pending
        end
    end

    def show
        client = Savon::Client.new do
            wsdl.document = WSDL_LOCATION
        end

        request = client.request "Judge.get_user" do
            soap.body = { :id_user => params[:id] }
        end

        response = request.to_hash[:judge_get_user_response]
        the_user = response[:user]
        @user = User.new
        @user.id = the_user[:id]
        @user.name = the_user[:name]
    end

    def index
        client = Savon::Client.new do
            wsdl.document = WSDL_LOCATION
        end

        request = client.request "Judge.get_all_users"
        @user_list = request[:judge_get_all_users_response][:users][:item]
    end

    private
    def user_params
        params.require(:user).permit(:name, :password, 
                                     :password_confirmation,
                                     :password_digest)
    end

end
