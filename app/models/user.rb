class User < ActiveRecord::Base
    VALID_NAME_REGEX = /\A[a-zA-Z][a-z0-9_.]*\Z/i
    VALID_PASS_REGEX = /\A[a-z0-9!"#$%&\/()]*\Z/i
    validates :name, presence: true,
                     length: { minimum: 4,  maximum: 16 },
                     format: { with: VALID_NAME_REGEX }

    has_secure_password
    # presence validation and confirmation are
    # automatically added by has_secure_password
    validates :password, length: { minimum: 6 },
                         format: { with: VALID_PASS_REGEX }

    def User.new_remember_token
        SecureRandom.urlsafe_base64
    end

    def User.encrypt(token)
        Digest::SHA1.hexdigest(token.to_s)
    end

end
